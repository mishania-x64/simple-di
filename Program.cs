﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace SimpleDI
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            // RegisterServices();
            DynamicallyResisterServices();
            IServiceScope scope = _serviceProvider.CreateScope();
            scope.ServiceProvider.GetRequiredService<ServiceConsumer>().CallService();
            DisposeServices();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();
            services.AddSingleton<SimpleDI.Abstractions.IService, SimpleDI.Implementation.Service>();
            services.AddSingleton<ServiceConsumer>();            
            _serviceProvider = services.BuildServiceProvider(true);
        }

        private static void DynamicallyResisterServices()
        {
            var services = new ServiceCollection();

            Assembly appAssembly = typeof(Program).Assembly;

            var appTypes =
                from type in appAssembly.GetTypes()
                where !type.IsAbstract
                where typeof(SimpleDI.Abstractions.IService).IsAssignableFrom(type)
                select type;

            foreach (var type in appTypes)
            {
                services.AddTransient(typeof(SimpleDI.Abstractions.IService), type);
            }

            services.AddSingleton<ServiceConsumer>();            
            _serviceProvider = services.BuildServiceProvider(true);
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}
