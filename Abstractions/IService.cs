using System;

namespace SimpleDI.Abstractions
{
    interface IService
    {
        void DoWork();
    }
}