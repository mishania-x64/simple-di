using System;

namespace SimpleDI
{
    class ServiceConsumer
    {
        private readonly SimpleDI.Abstractions.IService _service;
        public ServiceConsumer(SimpleDI.Abstractions.IService service)
        {
            // Concrete class injected using constructor injection.
            _service = service;
        }

        public void CallService()
        {
            _service.DoWork(); 
        }
    }
}