using System;

namespace SimpleDI.Implementation
{
    class Service : SimpleDI.Abstractions.IService
    {
        public void DoWork()
        {
            Console.WriteLine("Calling a concrete service...");
        }
    }
}